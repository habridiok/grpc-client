package com.gdn.grpc.client.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductRequest {

  private long id;
  private String sku;
  private String productName;
  private int stock;
  private long price;

}
