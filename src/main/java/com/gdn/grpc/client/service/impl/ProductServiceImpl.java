package com.gdn.grpc.client.service.impl;

import com.gdn.grpc.client.model.request.ProductRequest;
import com.gdn.grpc.client.model.response.ProductResponse;
import com.gdn.grpc.client.service.ProductService;
import com.gdn.grpc.client.service.helper.ProductServiceHelper;
import generatedproto.Product;
import generatedproto.ProductServiceGrpc.ProductServiceBlockingStub;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

  @Autowired
  ProductServiceBlockingStub productStub;

  @Autowired
  ProductServiceHelper productServiceHelper;

  @Override
  public ProductResponse getProductBySku(String sku) {
    Product.SkuRequest request = productServiceHelper.constructGrpcSkuRequest(sku);
    Product.ProductResponse response = productStub.getBySku(request);
    return this.productServiceHelper.convertGrpcResponseToProductResponse(response);
  }

  @Override
  public List<ProductResponse> getAllProduct() {
    Product.GetAllProductResponse response =
        productStub.getAllProduct(this.productServiceHelper.constructGrpcGetAllProductRequest());
    return this.productServiceHelper.convertGrpcGetAllResponseToProductResponses(response);
  }

  @Override
  public ProductResponse createProduct(ProductRequest productRequest) {
    Product.CreateProductRequest request =
        this.productServiceHelper.constructGrpcCreateProductRequst(productRequest);
    Product.ProductResponse response = this.productStub.createProduct(request);
    return this.productServiceHelper.convertGrpcResponseToProductResponse(response);
  }

  @Override
  public ProductResponse updateProduct(ProductRequest productRequest) {
    Product.UpdateProductRequest request =
        this.productServiceHelper.constructGrpcUpdateProductRequest(productRequest);
    Product.ProductResponse response = this.productStub.updateProduct(request);
    return this.productServiceHelper.convertGrpcResponseToProductResponse(response);
  }


}
