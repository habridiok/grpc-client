package com.gdn.grpc.client.service.helper;

import com.gdn.grpc.client.model.request.ProductRequest;
import com.gdn.grpc.client.model.response.ProductResponse;
import generatedproto.Product;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProductServiceHelper {

  public Product.SkuRequest constructGrpcSkuRequest(String sku) {
    return Product.SkuRequest.newBuilder().setSku(sku).build();
  }

  public ProductResponse convertGrpcResponseToProductResponse(
      Product.ProductResponse grpcResponse) {
    return ProductResponse.builder().id(grpcResponse.getId())
        .productName(grpcResponse.getProductname()).price(grpcResponse.getPrice())
        .sku(grpcResponse.getSku()).stock(grpcResponse.getStock()).build();
  }

  public Product.GetAllProductRequest constructGrpcGetAllProductRequest() {
    return Product.GetAllProductRequest.newBuilder().build();
  }

  public List<ProductResponse> convertGrpcGetAllResponseToProductResponses(
      Product.GetAllProductResponse grpcResponse) {
    return grpcResponse.getProductResponseList().stream()
        .map(this::convertGrpcResponseToProductResponse).collect(Collectors.toList());
  }

  public Product.CreateProductRequest constructGrpcCreateProductRequst(
      ProductRequest productRequest) {
    return Product.CreateProductRequest.newBuilder().setProductname(productRequest.getProductName())
        .setPrice(productRequest.getPrice()).setSku(productRequest.getSku())
        .setStock(productRequest.getStock()).build();
  }

  public Product.UpdateProductRequest constructGrpcUpdateProductRequest(
      ProductRequest productRequest) {
    return Product.UpdateProductRequest.newBuilder().setId(productRequest.getId())
        .setProductname(productRequest.getProductName()).setPrice(productRequest.getPrice())
        .setSku(productRequest.getSku()).setStock(productRequest.getStock()).build();
  }


}
