package com.gdn.grpc.client.service;

import com.gdn.grpc.client.model.request.ProductRequest;
import com.gdn.grpc.client.model.response.ProductResponse;

import java.util.List;

public interface ProductService {

    ProductResponse getProductBySku(String sku);
    List<ProductResponse> getAllProduct();
    ProductResponse createProduct(ProductRequest productRequest);
    ProductResponse updateProduct(ProductRequest productRequest);

}
