package com.gdn.grpc.client.controller;

import com.gdn.grpc.client.model.request.ProductRequest;
import com.gdn.grpc.client.model.response.ProductResponse;
import com.gdn.grpc.client.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/product")
public class ProductController {

  @Autowired
  ProductService productService;

  @GetMapping
  public ProductResponse getProductBySku(@RequestParam String sku) {
    return this.productService.getProductBySku(sku);
  }

  @GetMapping(value = "/getAll")
  public List<ProductResponse> getAllProduct() {
    return this.productService.getAllProduct();
  }

  @PostMapping(value = "/create")
  public ProductResponse createProduct(@RequestBody ProductRequest productRequest) {
    return this.productService.createProduct(productRequest);
  }

  @PutMapping(value = "/update")
  public ProductResponse updateProduct(@RequestBody ProductRequest productRequest) {
    return this.productService.updateProduct(productRequest);
  }

}
