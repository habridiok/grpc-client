package com.gdn.grpc.client.configuration;

import generatedproto.ProductServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GrpcStubConfig {

  private final String HOST = "localhost";
  private final int PORT = 9999;

  @Bean
  public ProductServiceGrpc.ProductServiceBlockingStub stubGrpcProductService() {
    ManagedChannel channel = ManagedChannelBuilder.forAddress(HOST, PORT).usePlaintext().build();
    return ProductServiceGrpc.newBlockingStub(channel);
  }

}
